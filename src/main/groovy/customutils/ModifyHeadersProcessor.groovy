package customutils

import nz.co.odinhealth.next.common.model.RoutingMessageKt
import nz.co.odinhealth.next.groovy.GroovyHelper
import nz.co.odinhealth.next.groovy.GroovyScriptProcessor
import nz.co.odinhealth.thor.model.RoutingMessage
import org.jetbrains.annotations.NotNull

class ModifyHeadersProcessor extends GroovyScriptProcessor{
    @Override
    RoutingMessage process(@NotNull RoutingMessage message) {
        RoutingMessage withAddedHeader = GroovyHelper.setHeader(message, "newHeader", "New value")
        RoutingMessage withRemovedHeader = GroovyHelper.removeHeader(withAddedHeader, "removedHeader")
        RoutingMessage withAppendedHeader = GroovyHelper.appendToHeader(withRemovedHeader, "modifiedHeader", "-extra content")
        def oldHeader = GroovyHelper.getHeader(withAppendedHeader, "willModify")
        //Can also get header directly from the RoutingMessage
        // def oldHeader = RoutingMessageKt.header(withAppendedHeader, "willModify")
        GroovyHelper.setHeader(withAppendedHeader, "willModify", oldHeader + "extra added")
    }
}
