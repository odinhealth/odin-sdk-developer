package customutils;

import nz.co.odinhealth.thor.model.MessageLetter
import nz.co.odinhealth.thor.model.RoutingMessage
import org.apache.groovy.json.internal.LazyMap

import static nz.co.odinhealth.next.groovy.GroovyHelper.*

class UtilsGroovy {

    private static RoutingMessage createMessage(String body){
        MessageLetter mainBody = new MessageLetter(body, new HashMap<String, String>())
        return new RoutingMessage(mainBody, null, null, null, new HashMap<String,String>())
    }


    public static Object useJsonMethod(String jsonText){
        json(jsonText, createMessage("testing"))
    }

    public static Object getAFromJson(String jsonText){
        LazyMap map = json(jsonText, createMessage("testing"))
        return map.get("a")
    }

    public static byte[] useEncryptMethod(String text){
        encrypt("key1", text.getBytes())
    }
}
