package customutils

import nz.co.odinhealth.next.groovy.GroovyScriptProcessor
import nz.co.odinhealth.next.groovy.GroovyHelper
import nz.co.odinhealth.thor.model.RoutingMessage
import org.jetbrains.annotations.NotNull

class ExternalProcessor extends GroovyScriptProcessor{
    @Override
    RoutingMessage process(@NotNull RoutingMessage message) {
        GroovyHelper.setBody(message, "bar")
    }
}
