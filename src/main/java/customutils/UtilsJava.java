package customutils;

import static nz.co.odinhealth.next.groovy.GroovyHelper.encrypt;
import static nz.co.odinhealth.next.groovy.GroovyHelper.json;

import java.util.HashMap;
import nz.co.odinhealth.thor.model.MessageLetter;
import nz.co.odinhealth.thor.model.RoutingMessage;
import org.apache.groovy.json.internal.LazyMap;

public class UtilsJava {

    private static RoutingMessage createMessage(String body){
        MessageLetter mainBody = new MessageLetter(body, new HashMap<String, String>());
        return new RoutingMessage(mainBody, null, null, null, new HashMap<String,String>());
    }


    public static Object useJsonMethod(String jsonText){
        return json(jsonText, createMessage("testing"));
    }

    public static Object getAFromJson(String jsonText){
        LazyMap map = (LazyMap)json(jsonText, createMessage("testing"));
        return map.get("a");
    }

    public static byte[] useEncryptMethod(String text){
        return encrypt("key1", text.getBytes());
    }

}