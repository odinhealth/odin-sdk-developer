package customutils;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;
import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import nz.co.odinhealth.next.common.crypto.CryptoSupport;
import nz.co.odinhealth.next.common.crypto.ProvidedSecretKey;
import nz.co.odinhealth.next.groovy.ApplicationContextHolder;
import org.apache.groovy.json.internal.LazyMap;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class UtilsTesting {

    @BeforeEach
    public void setupKeys(){
        CryptoSupport converterSupport = ApplicationContextHolder.getCryptoSupport();
        providedSecretKeys().forEach(converterSupport::createKey);
    }

    @Test
    public void testJson(){
        LazyMap body = (LazyMap) UtilsGroovy.useJsonMethod("{\"a\":1}");
        Assert.assertEquals(body.get("a"), 1);
    }

    @Test
    public void testEncrypt(){
        byte[] encrypted = UtilsGroovy.useEncryptMethod("test message");
    }

    //To use cryptoSupport, modify this method to add ProvidedSecretKeys
    private static List<ProvidedSecretKey> providedSecretKeys(){
        List<ProvidedSecretKey> secretKeys = new ArrayList<>();
        try {
            secretKeys.add(generateExampleKey());
        }catch(Exception e){
            e.printStackTrace();
        }
        return secretKeys;
    }

    private static ProvidedSecretKey generateExampleKey() throws NoSuchAlgorithmException, NoSuchPaddingException {
        KeyGenerator keyGenerator =  KeyGenerator.getInstance("AES");
        SecretKey secretKey = keyGenerator.generateKey();
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        byte[] iv = new byte[cipher.getBlockSize()];
        SecureRandom secureRandom = SecureRandom.getInstanceStrong();
        secureRandom.nextBytes(iv);
        IvParameterSpec ivParams = new IvParameterSpec((iv));
        return new ProvidedSecretKey("key1", secretKey.getEncoded(), "AES", "CBC", ivParams.getIV(), "PKCS5Padding");
    }


}
