# Odin sdk developer

This is an example project of how to develop using the next sdk.

### Key points from the pom.xml file

Parent of the project must be nz.co.odinhealth.next:groovy-helper-dependencies. This pulls in all 3rd party libraries that will be available when running groovy within the next engine. All the Dependencies are _provided_ scope. 

Dependency (of provided scope) on nz.co.odinhealth.next:groovy-helper . This provides the sdk methods and model classes available in next. Class with helper functions is named **GroovyHelper**

you can add any other 3rd party dependencies required and these will be pulled in transitively when your library is added to the next.

### Developing an external processor

To create a class that can be used within ExternalProcesor processor type in next, define a class that extends **nz.co.odinhealth.next.groovy.GroovyScriptProcessor**

## Packaging your libraries

Run a maven build to deploy your libraries to a repository that can be accessed by next engine. This will include just your library classes, and any transitive dependencies will be pulled in when your library is added as a dependency in next engine


## Setting up maven to use Odin's Artifactory (Maven repository)

Odin's Artifactory contains the parent pom and super jar required for development

Add this section to maven settings.xml file. Contact Odin for an account with username/password 

```
<server>
    <username>XXXXXX</username>
    <password>XXXXXX</password>
    <id>odin</id>
</server>
```
Add a profile that refers to Odin's Artifactory server 

```
<profile>
    <id>odin</id>
    <activation>
        <activeByDefault>false</activeByDefault>
    </activation>
    <repositories>
        <repository>
            <snapshots>
                <enabled>false</enabled>
            </snapshots>
            <id>central</id>
            <name>ext-release</name>
            <url>https://artifactory.ci.odinhealth.co.nz/artifactory/ext-release</url>
        </repository>
        <repository>
            <snapshots/>
            <id>snapshots</id>
            <name>ext-snapshot</name>
            <url>https://artifactory.ci.odinhealth.co.nz/artifactory/ext-snapshot</url>
        </repository>
    </repositories>
</profile>
```

Run a maven build to pull the artifacts. After doing this once you can work in an IDE

```
mvn -P odin clean package
```

